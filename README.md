Display the call stack from a C++ program like Python.

To retrieve the source code from your executable, the module uses under:
* Linux, the library "libbfd" (gcc flag [-rdynamic](http://gcc.gnu.org/onlinedocs/gcc/Link-Options.html) must be active)
* Windows, the library "Dbghelp.lib"

Of course the code must be compiled with **-g** or equivalent to produce debugging information.


For example, under linux, if your program throws an exception, your program will display:

```
#!Python
Traceback (most recent call last)
  File "/projects/traceback/main.cpp", line 18
    main
  File "/projects/traceback/main.cpp", line 13
    foo(int)
  File "/projects/traceback/main.cpp", line 11
    foo(int)
  File "/projects/traceback/exception.hpp", line 120
    traceback::Exception<std::runtime_error>::Exception(std::string const&)
  File "/projects/traceback/exception.hpp", line 69
    traceback::ErrorInfo::ErrorInfo(std::exception*, char const* (std::exception::*)() const)
ErrorInfo: An error message: [Errno  2] No such file or directory
```

Under Windows, your program will display:
```
#!Python
Traceback (most recent call last)
  File "c:\projects\traceback\main.cpp", line 18
    main
  File "c:\projects\traceback\main.cpp", line 13
    foo
  File "c:\projects\traceback\main.cpp", line 11
    foo
  File "c:\projects\traceback\exception.hpp", line 120
    traceback::Exception<std::runtime_error>::Exception<std::runtime_error>
  File "c:\projects\traceback\exception.hpp", line 69
    traceback::ErrorInfo::ErrorInfo
ErrorInfo: An error message: [Errno  2] No such file or directory
```

The code is based on the source code "addr2line" from the [binutils](http://www.gnu.org/software/binutils) library and from the [msdn](http://msdn.microsoft.com/en-us/library/windows/desktop/ms681412(v=vs.85).aspx) documentation.

To compile the test program, under Linux, you must run the following commands:
```
#!bash
g++ -g -c -rdynamic -Wall -Wextra traceback.cpp
g++ -g -c -rdynamic -Wall -Wextra main.cpp
g++ -rdynamic -g -Wall -Wextra -o test traceback.o main.o -lbfd
./test
```

Under windows:
```
#!bash
cl /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_UNICODE" /D "UNICODE" /Gm /EHsc /RTC1 /MDd /c /ZI /TP exception.cpp
cl /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_UNICODE" /D "UNICODE" /Gm /EHsc /RTC1 /MDd /c /ZI /TP main.cpp
link /DEBUG exception.obj main.obj Dbghelp.lib /out:test.exe
test
```